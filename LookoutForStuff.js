// Snooping around in JS :- 

// 1. Get all global-variables in a window which are added in current page.

/* First we add a hidden iframe;
 * then we test existing variables against the standard JavaScript API in the iframe;
 * then we remove the iframe.
 * based on : https://stackoverflow.com/a/49069050/12704100
 */
document.body.appendChild(document.createElement('div')).innerHTML='<iframe id="temoin" style="display:none"></iframe>';
Object.keys(window).filter(a => !(a in window.frames[window.frames.length-1])).sort().forEach((a,i) => console.log(i, a, window[a]));
document.body.removeChild($$('#temoin')[0].parentNode);

// 2. All global-variables which are touched(read) by current window.

/* Things that are null or undefined are usually not interesting to look at.
 * Most scripts will define a bunch of event handlers (i.e. functions) but they are also usually not interesting to dump out.
 * Properties on window that are set by the browser itself, are usually defined in a special way, and their property descriptors reflect that.
 * Globals defined with the assignment operator (i.e. window.foo = 'bar') have a specific-looking property descriptor, and we can leverage that.
 * Note, if the script defines properties using Object.defineProperty with a different descriptor, we'll miss them, but this is very rare in practice.
 * based on : https://stackoverflow.com/a/52693392/12704100
 */
 Object.keys(window).filter(x => typeof (window[x]) !== 'function' && 
    Object.entries( Object.getOwnPropertyDescriptor(window, x))
        .filter(e => ['value', 'writable', 'enumerable', 'configurable']
            .includes(e[0]) && e[1]).length === 4)
    